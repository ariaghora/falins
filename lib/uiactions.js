// globals
var gui = require('nw.gui');
var win = gui.Window.get();

function panelSelector(sender) {
    // reset properties
    $(".item").removeClass("active");
    $(".item").css({"height" : "40px"});    
    $(".subitem").css("display", "none");    

    // then set to active
    $(sender).addClass("active");

    if ($(sender).hasClass("item-master")) {
        $(sender).css({"height" : "100px"});
        $(".subitem").css("display", "block");                
    }

}

function subItemSelector(sender) {
    $(".subitem li").removeClass("active");
    $(sender).addClass("active");
}

function maximizeWindow(){    
    win.maximize();
}

function closeWindow(){
    win.close(true);
}

$(document).ready(function(){
    $(".item").css({"height" : "40px"});        
    $(".close").click(function(){
        alert("asd");
    }
    )
});